import React from "react";

function App() {
  const count = () => {
    for(let i = 100; i > 0; i--) {
      if(i%3===0) {
        console.log(i);
      }
    }
  }
  return (
    <div>
      <h1>Hi</h1>
      {count()}
    </div>
  );
}

export default App;
